package com.example.chessapp2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
public class MyView  extends View{
    private Drawable Board;
    public MyView(Context context) {
        super(context);
        Board = context.getResources().getDrawable(R.drawable.board);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect imageBounds = canvas.getClipBounds();  // Adjust this for where you want it

        Board.setBounds(imageBounds);
        Board.draw(canvas);

    }

}
