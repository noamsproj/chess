package com.example.chessapp2;

import androidx.appcompat.app.AppCompatActivity;
import com.example.chessapp2.Connecter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Button playBtn = (Button)findViewById(R.id.PlayBtn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent OfflineGamePage = new Intent(Menu.this, com.example.chessapp2.OfflineGameActivity.class);
                startActivity(OfflineGamePage);
            }
        });

    }
}