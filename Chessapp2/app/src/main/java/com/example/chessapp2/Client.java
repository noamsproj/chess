
package com.example.chessapp2;

import java.net.*;
import java.io.*;
import com.example.chessapp2.Connecter;

public class Client
{
    // initialize socket and input output streams
;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    // constructor to put ip address and port
    public Client(Socket clientSocket) throws IOException {
        // establish a connection
        try
        {
            this.clientSocket = clientSocket;
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        }
        catch(UnknownHostException u)
        {
            System.out.println(u);
        }
        catch(IOException i)
        {
            System.out.println(i);
        }

        while(true)
        {
            try {
                Connecter.GetMutex.acquire();
                try {
                    if(Connecter.sendData)
                    {
                        this.SendData(Connecter.DataFromClient);
                    }
                } finally {
                    Connecter.GetMutex.release();
                }
            } catch(InterruptedException ie) {
                // ...
            }
            try {
                Connecter.SendMutex.acquire();
                try {
                    if(Connecter.GetDataFromServer)
                    {
                        Connecter.DataFromServer = getData();
                    }
                } finally {
                    Connecter.SendMutex.release();
                }
            } catch(InterruptedException ie) {
                // ...
            }
        }



    }
    public void close()
    {
        // close the connection
        try
        {
            out.close();
            in.close();

        }
        catch(IOException i)
        {
            System.out.println(i);
        }
    }


  public String getData() throws IOException {

      return in.readLine();
  }

  public void SendData(String  data) throws  IOException {
        out.println(data);
  }

}