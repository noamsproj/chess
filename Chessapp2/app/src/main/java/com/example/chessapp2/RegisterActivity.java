package com.example.chessapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.chessapp2.Connecter;
import java.io.IOException;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button Create = (Button)findViewById(R.id.CreateBtn);
        EditText Username = (EditText)findViewById(R.id.getUserName);
        EditText Password = (EditText)findViewById(R.id.getPassword);
        EditText confirmPass = (EditText)findViewById(R.id.conPassword);
        EditText Email = (EditText)findViewById(R.id.Email);

        Create.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View V){
                if(Password.getText().toString().equals(confirmPass.getText().toString())
                        && !Username.getText().toString().isEmpty() &&
                        !Password.getText().toString().isEmpty() &&
                        !confirmPass.getText().toString().isEmpty() &&
                        !Email.getText().toString().isEmpty())
                {
                    String dataFromServer  = "";
                    String buffer = "";
                    buffer += "2{";
                    buffer += Email.getText().toString();
                    buffer+= ";" + Password.getText().toString();
                    buffer+= ";" + Username.getText().toString() + "}";
                    boolean isOk = false;
                    try {
                        Connecter.SendMutex.acquire();
                        try {

                            Connecter.sendData = true;
                            Connecter.DataFromClient = buffer;
                        } finally {
                            Connecter.SendMutex.release();
                        }
                    } catch(InterruptedException ie) {
                    }

                    try {
                        Connecter.GetMutex.acquire();
                        try {

                            Connecter.GetDataFromServer = true;
                            if (Connecter.DataFromServer.equals("200")) {
                                isOk = true;
                            }
                        } finally {
                            Connecter.GetMutex.release();
                        }
                    } catch(InterruptedException ie) {
                    }

                    if(isOk)
                    {
                        Intent MenuPage = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(MenuPage);
                    }
                    else
                    {
                        System.out.println(dataFromServer.substring(4));
                    }

                }
            }
    });
}
}