package com.example.chessapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.example.chessapp2.Connecter;
import com.example.chessapp2.R;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    private Socket socket;
    private String SERVER_IP = "10.0.2.2";
    private int SERVERPORT = 1234;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Thread(new ClientThread()).start();

        Button LoginBtn = (Button) findViewById(R.id.LoginBtn);
        Button RegisterBtn = (Button) findViewById(R.id.RegisterBtn);
        EditText Username = (EditText) findViewById(R.id.Username);
        EditText Password = (EditText) findViewById(R.id.Password);
        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                Intent RegisterPage = new Intent(MainActivity.this, com.example.chessapp2.RegisterActivity.class);
                startActivity(RegisterPage);
            }
        });
        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View V) {
                boolean isOk = false;
                if (!Username.getText().toString().isEmpty() &&
                        !Password.getText().toString().isEmpty()) {
                    String buffer = "1{" + Password.getText().toString() + ";" + Username.getText().toString() + "}";
                    try {
                        Connecter.SendMutex.acquire();
                        try {

                            Connecter.sendData = true;
                            Connecter.DataFromClient = buffer;
                        } finally {
                            Connecter.SendMutex.release();
                        }
                    } catch(InterruptedException ie) {
                    }

                    try {
                        Connecter.GetMutex.acquire();
                        try {

                            Connecter.GetDataFromServer = true;
                            if (!(Connecter.DataFromServer == null) && Connecter.DataFromServer.equals("200")) {
                                isOk = true;
                            } else {
                                System.out.println("hi");
                            }
                        } finally {
                            Connecter.GetMutex.release();
                        }
                    } catch(InterruptedException ie) {
                    }
                    if(isOk) {
                        Intent MenuPage = new Intent(MainActivity.this, Menu.class);
                        startActivity(MenuPage);
                    }
                }
            }
        });

    }

    class Connect extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {

                Socket clientSocket = new Socket("192.168.1.30", 1234);
                Client myClient = new Client(clientSocket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


    }
    class ClientThread implements Runnable {

        @Override
        public void run() {

            try {
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

                socket = new Socket(serverAddr, SERVERPORT);
                Client myClient = new Client(socket);
            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }

    }
}